
FROM node:20-alpine
WORKDIR /react-app
COPY ["./app/package.json", "./app/package-lock.json" , "./"]
RUN npm i --legacy-peer-deps 
EXPOSE 3000
COPY "./app" .
CMD [ "npm", "run", "start"]


